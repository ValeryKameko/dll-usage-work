#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

	typedef INT(*PFNREPLACEBYTES)(DWORD processId,
		SIZE_T replaceBytesSize, LPCBYTE replaceBytes,
		SIZE_T replacingBytesSize, LPCBYTE replacingBytes);

	typedef INT(*PFNREPLACESTRING)(DWORD processId, LPCSTR replaceString, LPCSTR replacingString);

	typedef INT(*PFNREPLACEWIDESTRING)(DWORD processId, LPCWSTR replaceString, LPCWSTR replacingString);

#ifdef __cplusplus
};
#endif