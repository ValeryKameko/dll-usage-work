﻿#include <iostream>
#include <Windows.h>
#include "DataReplacerHeader.h"

using namespace std;
const LPCWSTR DLL_PATH = L"Data Replacer.dll";
const LPCWSTR REPLACE_STRING = L"Hello it is very long string";
const LPCWSTR REPLACING_STRING = L"Short string [REPLACED]";

string GetErrorAsString(DWORD errorCode)
{
	if (errorCode == 0)
		return string();

	LPSTR message = nullptr;
	size_t messageSize = FormatMessageA(
		FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL, errorCode, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), reinterpret_cast<LPSTR>(&message), 0, NULL);

	string messageString{ const_cast<const LPCSTR>(message), messageSize };
	LocalFree(message);

	return messageString;
}

DWORD InvokeReplaceWideString(DWORD proccessId, LPCWSTR replaceString, LPCWSTR replacingString) {
	HMODULE library = LoadLibrary(DLL_PATH);
	if (!library) {
		DWORD errorCode = ::GetLastError();
		cout << "DLL loading error: " << GetErrorAsString(errorCode) << "\n";
		exit(-1);
	}

	PFNREPLACEWIDESTRING ReplaceWideString = (PFNREPLACEWIDESTRING)GetProcAddress(library, "ReplaceWideString");
	if (!ReplaceWideString) {
		DWORD errorCode = ::GetLastError();
		cout << "Function loading error: " << GetErrorAsString(errorCode) << "\n";
		exit(-1);
	}
	DWORD returnCode = ReplaceWideString(GetCurrentProcessId(), replaceString, replacingString);

	FreeLibrary(library);
	return returnCode;
}

int main() {

	LPWSTR str = _wcsdup(REPLACE_STRING);

	wcout << "String before replacement: " << str << "\n";

	InvokeReplaceWideString(GetCurrentProcessId(), REPLACE_STRING, REPLACING_STRING);

	wcout << "String after replacement: " << str << "\n";

	return 0;
}