﻿#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <numeric>
#include <algorithm>
#include <functional>

#include <Windows.h>
#include <Psapi.h>
#include <DataReplacer.h>
#include "DllInjector.h"

using namespace std;
const LPCSTR DEFAULT_INJECTING_LIBRARY_PATH = "Data Replacer.dll";

const LPCSTR REPLACE_BYTES_THREAD_FUNC_NAME = "ReplaceBytesThreadFunc";
const LPCSTR REPLACE_STRING_THREAD_FUNC_NAME = "ReplaceStringThreadFunc";
const LPCSTR REPLACE_WIDE_STRING_THREAD_FUNC_NAME = "ReplaceWideStringThreadFunc";


const LPCSTR DEFAULT_CALL_FUNC_NAME = REPLACE_BYTES_THREAD_FUNC_NAME;


const SIZE_T INJECTING_THREAD_STACK_SIZE = 1 << 20;

DWORD ReadProcessId()
{
	cout << "Enter process id: ";
	DWORD processId;
	cin >> processId;
	string line;
	getline(cin, line);
	return processId;
}

string ReadString(string promptValue, string defaultValue = "")
{
	string value;
	while (value.empty()) {
		cout << "Enter " << promptValue;
		if (!defaultValue.empty())
			cout << " (default '" << defaultValue << "')";
		cout << ": ";
		getline(cin, value);

		if (value.empty() && !defaultValue.empty())
			value = defaultValue;
	}
	return value;
}

wstring ReadWideString(string promptValue, wstring defaultValue = L"")
{
	wstring value;
	while (value.empty()) {
		cout << "Enter " << promptValue;
		if (!defaultValue.empty())
			wcout << " (default '" << defaultValue << "')";
		cout << ": ";
		getline(wcin, value);

		if (value.empty() && !defaultValue.empty())
			value = defaultValue;
	}
	return value;
}

LPCBYTE InjectReplaceStringParamValue(DllInjector &injector, DWORD processId, LPCSTR replaceString, LPCSTR replacingString)
{
	LPVOID replaceStringAddress = injector.injectData(reinterpret_cast<LPCBYTE>(replaceString), lstrlenA(replaceString) + 1);
	if (!replaceStringAddress)
		return nullptr;
	LPVOID replacingStringAddress = injector.injectData(reinterpret_cast<LPCBYTE>(replacingString), lstrlenA(replacingString) + 1);
	if (!replacingStringAddress)
		return nullptr;

	REPLACE_STRING_PARAM param;
	param.processId = processId;
	param.replaceString = reinterpret_cast<LPCSTR>(replaceStringAddress);
	param.replacingString = reinterpret_cast<LPCSTR>(replacingStringAddress);
	LPVOID paramAddress = injector.injectData(reinterpret_cast<LPCBYTE>(&param), sizeof(param));
	return reinterpret_cast<LPCBYTE>(paramAddress);
}

LPCBYTE InjectReplaceWideStringParamValue(DllInjector& injector, DWORD processId, LPCWSTR replaceString, LPCWSTR replacingString)
{
	LPVOID replaceStringAddress = injector.injectData(reinterpret_cast<LPCBYTE>(replaceString), lstrlenW(replaceString) * 2 + 2);
	if (!replaceStringAddress)
		return nullptr;
	LPVOID replacingStringAddress = injector.injectData(reinterpret_cast<LPCBYTE>(replacingString), lstrlenW(replacingString) * 2 + 2);
	if (!replacingStringAddress)
		return nullptr;

	REPLACE_WIDE_STRING_PARAM param;
	param.processId = processId;
	param.replaceString = reinterpret_cast<LPCWSTR>(replaceStringAddress);
	param.replacingString = reinterpret_cast<LPCWSTR>(replacingStringAddress);
	LPVOID paramAddress = injector.injectData(reinterpret_cast<LPCBYTE>(&param), sizeof(param));
	return reinterpret_cast<LPCBYTE>(paramAddress);
}

string GetProcessFileName(HANDLE process) {
	SIZE_T bufferSize = MAX_PATH;
	LPSTR buffer = reinterpret_cast<LPSTR>(alloca(bufferSize));
	GetProcessImageFileNameA(process, buffer, bufferSize);
	return buffer;
}

string GetLibraryFileName(HMODULE library) {
	SIZE_T bufferSize = MAX_PATH;
	LPSTR buffer = reinterpret_cast<LPSTR>(alloca(bufferSize));
	GetModuleFileNameA(library, buffer, bufferSize);
	return buffer;
}

LPCBYTE InjectReplaceBytesParamValue(
	DllInjector& injector, DWORD processId, 
	LPCBYTE replaceBytes, SIZE_T replaceBytesSize, 
	LPCBYTE replacingBytes, SIZE_T replacingBytesSize)
{
	LPVOID replaceBytesAddress = injector.injectData(replaceBytes, replaceBytesSize);
	if (!replaceBytesAddress)
		return nullptr;
	LPVOID replacingBytesAddress = injector.injectData(replacingBytes, replacingBytesSize);
	if (!replacingBytesAddress)
		return nullptr;

	REPLACE_BYTES_PARAM param;
	param.processId = processId;
	param.replaceBytes = reinterpret_cast<LPCBYTE>(replaceBytesAddress);
	param.replaceBytesSize = replaceBytesSize;
	param.replacingBytes = reinterpret_cast<LPCBYTE>(replacingBytesAddress);
	param.replacingBytesSize = replacingBytesSize;
	LPVOID paramAddress = injector.injectData(reinterpret_cast<LPCBYTE>(&param), sizeof(param));
	return reinterpret_cast<LPCBYTE>(paramAddress);
}



int main()
{
	DllInjector injector{};

	DWORD processId = ReadProcessId();
	if (injector.openProcess(processId)) {
		cout << "Open process error\n";
		return 1;
	}
	
	cout << "Victim process '" << GetProcessFileName(injector.getProcess()) << "' opened!\n";

	string libraryPath = ReadString("injecting library path", DEFAULT_INJECTING_LIBRARY_PATH);
	HMODULE library = LoadLibraryA(libraryPath.c_str());
	if (injector.injectLibrary(library)) {
		cout << "Library injection error\n";
		return 1;
	}
	cout << "Library '" << GetLibraryFileName(library) << "' injected!\n";

	string readFunctionNamePrompt = string("function name [\n") + 
		"\t" + REPLACE_BYTES_THREAD_FUNC_NAME + ",\n" +
		"\t" + REPLACE_STRING_THREAD_FUNC_NAME + ",\n" + 
		"\t" + REPLACE_WIDE_STRING_THREAD_FUNC_NAME + ",\n" +
		+ " or custom]";
	string functionName = ReadString(readFunctionNamePrompt, DEFAULT_CALL_FUNC_NAME);
	if (functionName == string(REPLACE_STRING_THREAD_FUNC_NAME)) {
		string replaceString = ReadString("replace string");
		string replacingString = ReadString("replacing string");
		LPCBYTE param = InjectReplaceStringParamValue(injector, processId, replaceString.c_str(), replacingString.c_str());
		injector.callFunction(library, REPLACE_STRING_THREAD_FUNC_NAME, (LPVOID)param, INJECTING_THREAD_STACK_SIZE);
	}
	else if (functionName == string(REPLACE_WIDE_STRING_THREAD_FUNC_NAME)) {
		wstring replaceString = ReadWideString("replace string");
		wstring replacingString = ReadWideString("replacing string");
		LPCBYTE param = InjectReplaceWideStringParamValue(injector, processId, replaceString.c_str(), replacingString.c_str());
		injector.callFunction(library, REPLACE_WIDE_STRING_THREAD_FUNC_NAME, (LPVOID)param, INJECTING_THREAD_STACK_SIZE);
	}
	else if (functionName == string(REPLACE_BYTES_THREAD_FUNC_NAME)) {
		string replaceString = ReadString("replace bytes");
		string replacingString = ReadString("replacing bytes");
		LPCBYTE param = InjectReplaceBytesParamValue(
			injector, processId, 
			reinterpret_cast<LPCBYTE>(replaceString.c_str()), replaceString.size(), 
			reinterpret_cast<LPCBYTE>(replacingString.c_str()), replacingString.size());
		injector.callFunction(library, REPLACE_BYTES_THREAD_FUNC_NAME, (LPVOID)param, INJECTING_THREAD_STACK_SIZE);
	}
	else {
		injector.callFunction(library, functionName.c_str(), (LPVOID)0, INJECTING_THREAD_STACK_SIZE);
	}
	system("pause");
	return 0;
}