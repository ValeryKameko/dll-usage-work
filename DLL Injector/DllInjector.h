#pragma once

#include <Windows.h>

class DllInjector final
{
public:
	DllInjector();
	~DllInjector();

	DWORD injectLibrary(HMODULE library);
	DWORD openProcess(DWORD processId);
	DWORD callFunction(HMODULE library, LPCSTR functionName, LPVOID paramValue, SIZE_T stackSize);
	LPVOID injectData(LPCBYTE paramValue, SIZE_T paramSize);

	HANDLE getProcess() const;

private:
	HANDLE process = nullptr;
	HMODULE kernel32 = nullptr;
	HMODULE library = nullptr;
};

