#include "DllInjector.h"
#include <string>
#include <Psapi.h>

constexpr DWORD DESIRED_PROCESS_ACCESS = PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE;
const LPCSTR KERNEL32_LIBRARY = "kernel32.dll";
constexpr SIZE_T INJECT_STACK_SIZE = 1 << 20;

LPSTR GetLibraryFileName(HMODULE library) {
	SIZE_T bufferSize = MAX_PATH;
	LPSTR buffer = reinterpret_cast<LPSTR>(malloc(bufferSize));
	GetModuleFileNameA(library, buffer, bufferSize);
	return buffer;
}


DllInjector::DllInjector()
{
}

DllInjector::~DllInjector()
{
	if (process)
		CloseHandle(process);
}

DWORD DllInjector::openProcess(DWORD processId) {
	process = ::OpenProcess(DESIRED_PROCESS_ACCESS, false, processId);
	kernel32 = GetModuleHandleA(KERNEL32_LIBRARY);
	return (process && kernel32) ? ERROR_SUCCESS : ERROR;
}

DWORD DllInjector::injectLibrary(HMODULE library)
{
	LPSTR libraryPath = GetLibraryFileName(library);
	SIZE_T libraryLength = lstrlenA(libraryPath) + 1;
	LPVOID paramAddress = nullptr;
	if (!(paramAddress = injectData(reinterpret_cast<LPCBYTE>(libraryPath), libraryLength)))
		return ERROR;
	DWORD result = callFunction(this->kernel32, "LoadLibraryA", paramAddress, INJECT_STACK_SIZE);
	if (!result)
		return ERROR;
	return ERROR_SUCCESS;
}

DWORD DllInjector::callFunction(HMODULE library, LPCSTR functionName, LPVOID paramValue, SIZE_T stackSize)
{
	DWORD result;

	LPTHREAD_START_ROUTINE loadLibraryFunction = reinterpret_cast<LPTHREAD_START_ROUTINE>(GetProcAddress(library, functionName));
	HANDLE remoteThread = CreateRemoteThread(this->process, nullptr, stackSize, loadLibraryFunction, paramValue, 0, NULL);
	if (!remoteThread)
		return ERROR;

	WaitForSingleObject(remoteThread, INFINITE);
	if (!GetExitCodeThread(remoteThread, &result)) {
		CloseHandle(remoteThread);
		return ERROR;
	}
	CloseHandle(remoteThread);
	return result;
}

LPVOID DllInjector::injectData(LPCBYTE paramValue, SIZE_T paramSize)
{
	LPVOID paramAddress = VirtualAllocEx(this->process, 0, paramSize + 1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (!paramAddress)
		return nullptr;
	BOOL result = WriteProcessMemory(this->process, paramAddress, paramValue, paramSize + 1, NULL);
	if (!result)
		return nullptr;
	return paramAddress;
}

HANDLE DllInjector::getProcess() const
{
	return process;
}
