﻿#include <iostream>
#include <sstream>
#include <Windows.h>
#include "DLL injector victim.h"

using namespace std;
constexpr LPCSTR REPEATING_STRING = "Repeating string...";
constexpr DWORD SLEEP_TIME = 1000;

LPSTR DuplicateString(LPCSTR string) {
	SIZE_T stringLength = strlen(string) + 1;
	LPVOID duplicatedStringBytes = malloc(stringLength * sizeof(WCHAR));
	LPSTR duplicatedString = reinterpret_cast<LPSTR>(duplicatedStringBytes);
	memcpy_s(duplicatedString, stringLength, string, stringLength);
	return duplicatedString;
}

void SetTitle() {
	DWORD processId = GetCurrentProcessId();
	wostringstream wss{};
	wss << "Proccess id: " << processId;
	SetConsoleTitle(wss.str().c_str());
}

int main()
{
	SetTitle();

	LPCSTR duplicatedString = DuplicateString(REPEATING_STRING);
	while (true) {
		cout << duplicatedString;
		cout << endl;
		Sleep(SLEEP_TIME);
	}
	return 0;
}