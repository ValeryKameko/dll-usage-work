﻿#include <iostream>
#include <windows.h>
#include <DataReplacer.h>

using namespace std;
const char* STRING_VALUE = "Intial string value";
const char* REPLACING_STRING_VALUE = "c0rrupt3d v41ue";


int main()
{
	char* heapString = _strdup(STRING_VALUE);
	cout << "Initial string value: " << heapString << "\n";

	cout << "[Start string replacing...]\n";
	ReplaceString(GetCurrentProcessId(), STRING_VALUE, REPLACING_STRING_VALUE);
	cout << "[End string replacing...]\n";
	
	cout << "New string value: " << heapString << "\n";
	return 0;
}
