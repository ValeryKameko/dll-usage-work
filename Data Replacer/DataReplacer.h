#pragma once

#include <Windows.h>

#ifdef DATAREPLACER_EXPORTS
#	define DATAREPLACER_API __declspec(dllexport)
#else
#	define DATAREPLACER_API __declspec(dllimport)
#endif // DATAREPLACER_EXPORTS

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct {
		DWORD processId;
		LPCWSTR replaceString;
		LPCWSTR replacingString;
	} *PREPLACE_WIDE_STRING_PARAM, REPLACE_WIDE_STRING_PARAM;

	typedef struct {
		DWORD processId;
		LPCSTR replaceString;
		LPCSTR replacingString;
	} *PREPLACE_STRING_PARAM, REPLACE_STRING_PARAM;

	typedef struct {
		DWORD processId;
		SIZE_T replaceBytesSize; 
		LPCBYTE replaceBytes;
		SIZE_T replacingBytesSize; 
		LPCBYTE replacingBytes;
	} *PREPLACE_BYTES_PARAM, REPLACE_BYTES_PARAM;


	DATAREPLACER_API INT ReplaceBytes(
		DWORD processId,
		SIZE_T replaceBytesSize, LPCBYTE replaceBytes,
		SIZE_T replacingBytesSize, LPCBYTE replacingBytes);

	DATAREPLACER_API INT ReplaceBytesThreadFunc(PREPLACE_BYTES_PARAM pTask);

	DATAREPLACER_API INT ReplaceString(DWORD processId, LPCSTR replaceString, LPCSTR replacingString);

	DATAREPLACER_API INT ReplaceStringThreadFunc(PREPLACE_STRING_PARAM pTask);

	DATAREPLACER_API INT ReplaceWideString(DWORD processId, LPCWSTR replaceString, LPCWSTR replacingString);

	DATAREPLACER_API INT ReplaceWideStringThreadFunc(PREPLACE_WIDE_STRING_PARAM pTask);
	

#ifdef __cplusplus
}
#endif
