#include <malloc.h>
#include <memory.h>

#include "DataReplacer.h"
#include <iostream>
#include <iomanip>

constexpr INT NeedProcessAccess = PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_QUERY_INFORMATION;
constexpr INT NeedMemoryRegionProtect = PAGE_READWRITE;
constexpr INT NeedMemoryRegionState = MEM_COMMIT;
constexpr INT NeedMemoryRegionType = MEM_PRIVATE | MEM_MAPPED;

typedef struct PROCESS_MEMORY_REGION_TASK {
	HANDLE hProcess;

	SIZE_T replaceBytesSize;
	LPCBYTE replaceBytes;
	SIZE_T replacingBytesSize;
	LPCBYTE replacingBytes;

	PSSIZE_T prefixArray;
	LPBYTE page;
	SIZE_T pageSize;
} *PPROCESS_MEMORY_REGION_TASK, PROCESS_MEMORY_REGION_TASK;

static BOOL IsMemoryRegionHaveSuitableAccess(const MEMORY_BASIC_INFORMATION* memInfo);

static void CalculatePrefixFunction(PSSIZE_T prefixArray, LPCBYTE bytes, SIZE_T bytesSize);

static void ProccessMemoryRegion(PMEMORY_BASIC_INFORMATION pMemInfo, PPROCESS_MEMORY_REGION_TASK pTask, PSSIZE_T pPrefixIndex);

static SSIZE_T CalculatePrefixTransition(PPROCESS_MEMORY_REGION_TASK pTask, SSIZE_T prefixIndex, BYTE byte);

DATAREPLACER_API INT ReplaceBytes(
	DWORD processId,
	SIZE_T replaceBytesSize, LPCBYTE replaceBytes,
	SIZE_T replacingBytesSize, LPCBYTE replacingBytes)
{
	std::clog << "Starting replacing " 
		<< (LPCSTR)replaceBytes << "[" << replaceBytesSize << "]"
		<< " -> " 
		<< (LPCSTR)replacingBytes << "[" << replacingBytesSize << "]" << std::endl;

	std::clog << "Opening process" << std::endl;
	HANDLE hProcess = OpenProcess(NeedProcessAccess, FALSE, processId);
	if (!hProcess)
		return ERROR_ACCESS_DENIED;
	std::clog << "Process opened" << std::endl;

	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);

	std::clog << "Calculating prefix function array" << std::endl;
	PSSIZE_T prefixFunctionArray = reinterpret_cast<PSSIZE_T>(alloca(replaceBytesSize * sizeof(SSIZE_T)));
	std::clog << "Calculated prefix function array" << std::endl;

	SSIZE_T currentPrefixIndex = -1;
	CalculatePrefixFunction(prefixFunctionArray, replaceBytes, replaceBytesSize);

	PROCESS_MEMORY_REGION_TASK task;
	task.hProcess = hProcess;
	task.prefixArray = prefixFunctionArray;
	task.replaceBytes = replaceBytes;
	task.replaceBytesSize = replaceBytesSize;
	task.replacingBytes = replacingBytes;
	task.replacingBytesSize = replacingBytesSize;
	task.page = reinterpret_cast<LPBYTE>(alloca(sysInfo.dwPageSize));
	task.pageSize = sysInfo.dwPageSize;

	LPBYTE address = 0;
	MEMORY_BASIC_INFORMATION memInfo;
	do {
		VirtualQueryEx(hProcess, address, &memInfo, sizeof(memInfo));
		ProccessMemoryRegion(&memInfo, &task, &currentPrefixIndex);
		address += memInfo.RegionSize;
	} while (address != 0);
	return ERROR_SUCCESS;
}

DATAREPLACER_API INT ReplaceBytesThreadFunc(PREPLACE_BYTES_PARAM pTask)
{
	return ReplaceBytes(
		pTask->processId,
		pTask->replaceBytesSize, pTask->replaceBytes,
		pTask->replacingBytesSize, pTask->replacingBytes);
}

DATAREPLACER_API INT ReplaceString(DWORD processId, LPCSTR replaceString, LPCSTR replacingString)
{
	SIZE_T replaceStringSize = lstrlenA(replaceString);
	SIZE_T replacingStringSize = lstrlenA(replacingString) + 1;
	return ReplaceBytes(processId, replaceStringSize, (PBYTE)replaceString, replacingStringSize, (PBYTE)replacingString);
}

DATAREPLACER_API INT ReplaceStringThreadFunc(PREPLACE_STRING_PARAM pTask)
{
	return ReplaceString(pTask->processId, pTask->replaceString, pTask->replacingString);
}

DATAREPLACER_API INT ReplaceWideString(DWORD processId, LPCWSTR replaceString, LPCWSTR replacingString)
{
	SIZE_T replaceStringSize = lstrlenW(replaceString) * 2;
	SIZE_T replacingStringSize = (lstrlenW(replacingString) + 1) * 2;
	return ReplaceBytes(processId, replaceStringSize, (PBYTE)replaceString, replacingStringSize, (PBYTE)replacingString);
}

DATAREPLACER_API INT ReplaceWideStringThreadFunc(PREPLACE_WIDE_STRING_PARAM pTask)
{
	return ReplaceWideString(pTask->processId, pTask->replaceString, pTask->replacingString);
}


static void CalculatePrefixFunction(PSSIZE_T prefixArray, LPCBYTE bytes, SIZE_T bytesSize)
{
	prefixArray[0] = -1;

	SSIZE_T currentIndex = -1;
	for (SIZE_T index = 1; index < bytesSize; index++) {
		while (currentIndex != -1 && bytes[index] != bytes[currentIndex + 1]) {
			currentIndex = prefixArray[currentIndex];
		}
		if (bytes[index] == bytes[currentIndex + 1])
			currentIndex++;
		prefixArray[index] = currentIndex;
	}
}

static BOOL IsMemoryRegionHaveSuitableAccess(const MEMORY_BASIC_INFORMATION * memInfo)
{
	return
		(memInfo->State & NeedMemoryRegionState) &&
		(memInfo->Type & NeedMemoryRegionType) &&
		((memInfo->Protect & NeedMemoryRegionProtect) == NeedMemoryRegionProtect);
}

static void ProccessMemoryRegion(PMEMORY_BASIC_INFORMATION pMemInfo, PPROCESS_MEMORY_REGION_TASK pTask, PSSIZE_T pPrefixIndex)
{
	if (!IsMemoryRegionHaveSuitableAccess(pMemInfo)) {
		*pPrefixIndex = -1;
		return;
	}

	std::clog << "Processing memory region ";
	std::clog << std::setw(16) << std::hex << (SIZE_T)pMemInfo->BaseAddress << " -- ";
	std::clog << std::setw(16) << std::hex << (SIZE_T)(reinterpret_cast<LPBYTE>(pMemInfo->BaseAddress) + pMemInfo->RegionSize) << std::endl;

	for (SIZE_T pageIndex = 0, byteIndex = 0; byteIndex < pMemInfo->RegionSize; pageIndex++) {
		SIZE_T readBytes;
		LPBYTE address = reinterpret_cast<LPBYTE>(pMemInfo->BaseAddress) + byteIndex;
		if (address == pTask->page) {
			address += pTask->pageSize;
			continue;
		}
		ReadProcessMemory(pTask->hProcess, address, pTask->page, pTask->pageSize, &readBytes);
		for (SIZE_T pageByteIndex = 0; pageByteIndex < pTask->pageSize; pageByteIndex++, byteIndex++) {
			*pPrefixIndex = CalculatePrefixTransition(pTask, *pPrefixIndex, pTask->page[pageByteIndex]);
			if ((*pPrefixIndex + 1) == pTask->replaceBytesSize) {
				LPBYTE startAddress = address + pageByteIndex - pTask->replaceBytesSize + 1;
				if (startAddress != pTask->replacingBytes && startAddress != pTask->replaceBytes) {
					std::clog << "Substituting string ";
					std::clog << std::setw(16) << std::hex << (SIZE_T)startAddress << std::endl;

					SIZE_T replacedCount = 0;
					bool result = WriteProcessMemory(
						pTask->hProcess,
						startAddress,
						pTask->replacingBytes, pTask->replacingBytesSize, &replacedCount);
				}
				else {
					std::clog << "Our string skip ";
					std::clog << std::setw(16) << std::hex << (SIZE_T)(address + pageByteIndex - pTask->replaceBytesSize) << std::endl;
				}
				*pPrefixIndex = -1;
			}
		}
	}
}

static SSIZE_T CalculatePrefixTransition(PPROCESS_MEMORY_REGION_TASK pTask, SSIZE_T prefixIndex, BYTE byte)
{
	while (prefixIndex != -1 && pTask->replaceBytes[prefixIndex + 1] != byte) {
		prefixIndex = pTask->prefixArray[prefixIndex];
	}
	if (pTask->replaceBytes[prefixIndex + 1] == byte) {
		prefixIndex++;
		if (byte == 'l') {
			return prefixIndex;
		}
	}
	return prefixIndex;
}